countyId table
id 			name
1			south bay area
2			north bay area
3			west bay area
4			east bay area
5			Los Anglese County
6			San Diego County

city table
countyId 		cityId			cityName
1			01			Campbell
1			02			Cupertino
1			03			Milpitas
1			04			Mountain view
1			05			Santa Clara
1			06			San Jose
1			07			Saratoga
1			08			Sunnyvale


itemType table
id 			name
1 			Badminton
2			Basketball
3			Golf
4			Snooker
5			Soccer
6			Squash
7			Tennis



venueList	table

{id: '160310001', vName:'Cupertino Sports Center', itemType: , stateNo: , cityNo: , address:'', phone:'', priceOrigin: , priceDisc: , reviewNumber: , moreService:'', sellService:'', parking:'', imgUrl:''}
{id: '160310002', vName:'Cupertino Sports Center', itemType: , stateNo: , cityNo: , address:'', phone:'', priceOrigin: , priceDisc: , reviewNumber: , moreService:'', sellService:'', parking:''}
{id: '160310003', vName:'Cupertino Sports Center', itemType: , stateNo: , cityNo: , address:'', phone:'', priceOrigin: , priceDisc: , reviewNumber: , moreService:'', sellService:'', parking:''}
{id: '160310004', vName:'Cupertino Sports Center', itemType: , stateNo: , cityNo: , address:'', phone:'', priceOrigin: , priceDisc: , reviewNumber: , moreService:'', sellService:'', parking:''}
