/**
 * Created by AlecNing on 3/15/16.
 */
app.controller('bookController', ['$scope', function($scope) {
    $scope.myInterval = 3000;
    $scope.slides = [
        {
            image:
                '../../style/images/venue1/venue1pic1.jpg'
        },
        {
            image:
                '../../style/images/venue2/venue2pic1.jpg'
        },
        {
            image:
                '../../style/images/venue3/venue3pic1.jpg'
        }
    ];

}]);
